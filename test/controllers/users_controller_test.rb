require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:tanaka)
    @other_user = users(:suzuki)
  end

  test "should get new" do
    get signup_path
    assert_response :success
  end

  # minitestにRSpecのdescribeとかcontextに該当するものがないか今度調べる

  test "#edit - 未ログイン時はログインページへ遷移" do
    get edit_user_path(@user)
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "#edit - 別ユーザーの場合、rootへ遷移" do
    log_in_as(@other_user)
    get edit_user_path(@user)
    assert flash.empty?
    assert_redirected_to root_url
  end

  test "#update - 未ログイン時はログインページへ遷移" do
    patch user_path(@user), params: { user: {
      name: @user.name,
      email: @user.email
    } }
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "#update - 別ユーザーの場合、rootへ遷移" do
    log_in_as(@other_user)
    patch user_path(@user), params: { user: {
      name: @user.name,
      email: @user.email
    } }
    assert flash.empty?
    assert_redirected_to root_url
  end

  test "#update - adminのtrueへの更新を禁止" do
    log_in_as(@other_user)
    assert_not @other_user.admin?
    patch user_path(@other_user), params: {
      user: {
        password: "password",
        password_confirmation: "password",
        admin: true
      }
    }
    assert_not @other_user.reload.admin?
  end

  test "#index - 未ログイン時はログインページへ遷移" do
    get users_path
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "#destroy - 未ログインの時はログインページへ遷移" do
    assert_no_difference 'User.count' do
      delete user_path(@user)
    end
    assert_redirected_to login_url
  end

  test "#destroy - adminユーザーでない時はrootへ遷移" do
    log_in_as(@other_user)
    assert_no_difference 'User.count' do
      delete user_path(@user)
    end
    assert_redirected_to root_path
  end
end
