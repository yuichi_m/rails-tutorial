require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:tanaka)
  end

  test "ユーザー情報の更新に失敗する" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), params: { user: {
      name: "",
      email: "foo@invalid",
      password: "foo",
      password_confirmaiton: "bar"
    } }
    assert_template 'users/edit'
  end

  test "ユーザー情報の更新に成功する with フレンドリーフォワーディング" do
    get edit_user_path(@user)
    log_in_as(@user)
    assert_redirected_to edit_user_path(@user)
    name = "Saburo Takahashi"
    email = "changed@test.com"
    patch user_path(@user), params: { user: {
      name: name,
      email: email,
      password: "", # パスワードを指定しない場合、パスワードは変更されない
      password_confirmaiton: ""
    } }
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal name, @user.name
    assert_equal email, @user.email
  end
end
