require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest

  def setup
    ActionMailer::Base.deliveries.clear
  end

  test "入力値が誤っている場合、ユーザーは登録されない" do
    get signup_path

    assert_no_difference 'User.count' do
      post users_path, params: { user: { name:  "",
        email:                 "user@invalid",
        password:              "foo",
        password_confirmation: "bar" } }
    end

    assert_template 'users/new'
    assert_select 'div#error_explanation'
    assert_select 'div.field_with_errors'
  end

  test "入力値が正しい場合、ユーザーが登録される" do
    get signup_path

    assert_difference 'User.count', 1 do
      post users_path, params: { user: {
        name:                  "Jiro Sato",
        email:                 "sato@test.com",
        password:              "test01",
        password_confirmation: "test01" } }
    end

    assert_equal 1, ActionMailer::Base.deliveries.size
    user = assigns(:user)
    assert_not user.activated?
    # 有効化していない状態でログイン
    log_in_as(user)
    assert_not is_logged_in?
    # 有効化トークンが不正の場合
    get edit_account_activation_path('Invalid token', email: user.email)
    assert_not is_logged_in?
    # 有効化トークンが正しい場合
    get edit_account_activation_path(user.activation_token, email: user.email)
    assert user.reload.activated?
    follow_redirect!
    assert_template 'users/show'
    assert_select 'div.alert-success'
    assert is_logged_in?
  end
end
